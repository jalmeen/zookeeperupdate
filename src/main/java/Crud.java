import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;
import java.util.List;

/**
 * Created by joy on 7/9/17.
 */
public class Crud extends Connection {
    private String path;

    Connection zk_connection = new Connection();

    //initializing connection
    public ZooKeeper intialize() {

        try {
            zoo = zk_connection.connect("192.168.1.14:2180");
            System.out.println(zoo);
            return zoo;
        } catch (Exception e) {
            System.out.println(e.getMessage()); // Catches error messages
        }
        return zoo;
    }

    //create parent znode
    public void create(String path, byte[] data) throws KeeperException, InterruptedException {
        zoo.create(path, data, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
    }

    //createChildren
    public void createChildren(String parent, String child, byte[] data) throws Exception {
        String path = parent + "/" + child + "-";

        System.out.println("created child node with path " + zoo.create(path, data, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL_SEQUENTIAL));
    }

    // node version
    public Stat getZNodeStats(String path) throws KeeperException, InterruptedException {
        Stat stat = zoo.exists(path, true);
        if (stat != null) {
            System.out.println("Node exists and the node version is " + stat.getVersion());
        }
        else {
            System.out.println("Node does not exists");
        }
        return stat;
    }

    //update the value
    public void update(String path, byte[] data) throws KeeperException, InterruptedException {
        int version = zoo.exists(path, true).getVersion();
        zoo.setData(path, data, version);
        System.out.println("Updated data");
    }

    //read the list of children
    public void list(String znode) throws Exception {
        String path = znode;
        List<String> children = zoo.getChildren(path, new Watcher() {

            public void process(WatchedEvent event) {
                if (event.getType() == Event.EventType.NodeChildrenChanged) {
                    System.out.println("Children nodes are created");
                }
            }
        });

        if (children.isEmpty()) {
            System.out.println("no children in the znode");
            return;
        }
        System.out.println("Childrens : " + children + " : " + children.size());
    }


    //delete znode
    public void delete(String znode) throws Exception {
        String path = znode;
        List<String> childNodes = zoo.getChildren(path, false);

        //remove the childNodes first
        for (String childNode : childNodes) {
            zoo.delete(path + "/" + childNode, -1);
        }

        zoo.delete(path, -1);
        System.out.println("Done deleting");
    }

    public void close() throws InterruptedException {
        zoo.close();
    }

}

