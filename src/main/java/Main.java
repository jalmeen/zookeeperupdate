import org.apache.zookeeper.data.Stat;

/**
 * Created by joy on 7/9/17.
 */
public class Main {
    public static void main(String[] args) throws Exception {
        // Assign path to znode
        String pathFordata = "/minio";

        // data in byte array
        byte[] dataInitial = "Minion Family".getBytes();
        String value = new String(dataInitial, "UTF-8");

        byte[] updatedData = "Members".getBytes();
        String uValue = new String(updatedData,"UTF-8");

        Crud crud = new Crud();
        crud.intialize();

        crud.create(pathFordata, dataInitial);

        crud.getZNodeStats(pathFordata);

        System.out.println("Path :" + pathFordata + " Data: " + value);

        crud.createChildren(pathFordata,"stuart","Member1".getBytes());
        crud.createChildren(pathFordata,"jack","Member2".getBytes());


        crud.list(pathFordata);

        crud.update(pathFordata,updatedData);
        System.out.println("Updated value is : " + uValue);

        crud.delete(pathFordata);

        crud.close();
    }
}
